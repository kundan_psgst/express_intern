var express = require('express');
var router = express.Router();
var ageservice = require("../services/ageService")

router.get('/', function(req, res, next) {
    res.send(ageservice.getage(req.body));
});
module.exports = router;