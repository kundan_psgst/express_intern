var express = require('express');
var router = express.Router();
var bmiservice = require('../services/bmiservice');

//Middleware
router.use(function(req,res,next){
    if(req.body.height == null || req.body.weight == null)
    {
        res.send({err:"Check your data"});
        console.log("Caught by middleware");
    }
    next();
});

//methods
router.post('/',function(req,res,next){
    res.send(bmiservice.bmi(req.body));
});
module.exports = router;
