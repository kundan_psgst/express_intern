function bmi(data){
    var height = data.height/100;
    var weight = data.weight;

    var bmi = Number(weight/(height**2).toFixed(2));

    if(bmi){
        return {bmi:bmi};
    }
    else{return {err:"Wrong values"}}
}

module.exports.bmi = bmi;