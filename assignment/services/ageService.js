
function getage(data){
    var dob = new Date(data.year,data.month-1,data.day);
    var month_diff = Date.now() - dob.getTime();
    var age_dt = new Date(month_diff);
    var year = age_dt.getUTCFullYear();
    var age = Math.abs(year - 1970);
    if(age>0)
    {
        return {age:age};
    }
    else{return {err:"Date isnt right",age:age}}
};

module.exports.getage = getage;